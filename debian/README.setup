HOW TO SETUP iTALC (with the example of a Debian Edu network setting)
=====================================================================
... provided by Valerio Pachera (on 30th Dec 2008), see #511387
... edited, revied and modified by Mike Gabriel (during DebConf2013)


This document attempts at explaining how iTALC could be / is
pre-configured on a Debian Edu / Skolelinux setup.

The point is that different actions have to be taken on the base of
the different Debian Edu installation profiles we are working with.

Debian Edu Profiles:
    -main-server (TJENER)
    -ltsp-server (Terminal Server based on LTSP)
    -workstation
    -diskless workstation (DLWs)

Short introduction:

    - iTALC consists of three programs:
        1 - the client (called "ica"). It is a daemon that runs on the computers
            we want to control (also called clients)
        2 - the main application "iTALC" that teachers use to check and control
            students.
        3 - Since iTALC version 2.0.0 there is a third component: imc (iTALC
            Management Console)
    - iTALC uses three SSL keypairs to increase security.
    - iTALC works with a role model, known roles are: teacher, supporter and
      admin. A fourth implicit role is the student role.

The private SSL keys have to be present on the computers where teachers
want to run the main application.

Actually we don't know which computer the teachers will be using after all,
so the recommendation is to make the private keys available on all
computers/profiles and protect them via file permissions.

The public SSL keys, of course, have to be present on all computers and must be
readable to anyone (or--at least--all students). The italc-client Debian package
assumes that there is a fourth role called student on every system. Users
in the role of students get access to the public SSL keys, users that are
in neither of those italc-* groups cannot use italc at all.

(Make sure that teachers, supporters and administrators are also members in the group
representing the role of a student).

The SSL key creation is (since iTALC Debian package version 2.0.0-1) fully
handled during package configuration.

The Debian package italc-client adds four groups to the system. The default Posix
group names are:

  italc-admin
  italc-supporter
  italc-teacher
  italc-student

The SSL keys then get created during package configuration via the
imc -createkeypair command and the files get protected with appropriate
file permissions. All steps are configurable through debconf or through
preseeding. The key creation can also be deactivated completely if
you prefer a manual setup.

For Debian Edu / Skolelinux, a recommended preseeding set is recommended:

italc-client/create-keypairs true
italc-client/create-groups-for-roles false
italc-client/use-existing-groups-for-roles true
italc-client/group-italc-teacher teachers
italc-client/group-italc-student students
italc-client/group-italc-supporter admins
italc-client/group-italc-admin admins
italc-client/key-access-for-groups true

If the package italc-client is preseeded while Debian Edu's LDAP is still
down (during main-server installation), then use the numeric gidNumbers
instead of group names.

So in Debian Edu the default italc-* Posix groups get overridden as shown
below:

  italc-teacher -> teachers
  italc-student -> student
  italc-supporter -> admins
  italc-admins -> admins

Once, the italc-client package is installed and configured, the only thing
left is providing access to the iTALC SSL keys from every client machine
on the Debian Edu network.


### THOUGHTS ON KEY GENERATION (Debian Edu specific) ###

iTALC keys in a Debian Edu setup have to be generated once and be available to
all clients. One good way to do that is generating them on the "main-server"
profile/machine because every computer on the Debian Edu network is in
contact with it.

-- MAIN-SERVER PROFILE --

During package installation keys will get created in the directory

  /etc/italc/keys/

This folder contains subfolders of the names "public" and "private"
containing the respective keys.

The SSL key access is controlled via Posix file permissions. Just assign users
in the different iTALC roles to the corresponding Posix groups.

Then we have to make the keys available to the other hosts on the network,
so we e.g. export them using NFSv4 with something like that in /etc/exports

    file=/etc/exports
    """
    /srv/nfs4/etc/italc @ltsp-server-hosts(sec=krb5p:krb5i:krb5:sys,rw,sync,no_subtree_check) @workstation-hosts(sec=krb5p:krb5i:krb5:sys,rw,sync,no_subtree_check)
    """

Make sure that /etc/italc is `bind-mounted' to /srv/nfs4/etc/italc from within
/etc/fstab on the main-server machine:

    file=/etc/fstab
    """
    /etc/italc       /srv/nfs4/etc/italc none    bind    0       0
    """

To activate this new configuration, enter this command sequence:

    $ mkdir /srv/nfs4/etc
    $ mount -a
    $ exportfs -ar


-- THIN CLIENTS --

If the main-server TJENER also serves as an LTSP server, then we
do not need any further work to be done. Keys are already in place.

If the LTSP server is a separate machine, then see below and follow the
description for workstations.


-- WORKSTATION --

Both iTALC client (italc-client) and iTALC master (italc-master) have to be
installed by default on this Debian Edu installation profile.

We need the same keys that are on the MAIN server. We simply have to
create the folder /etc/italc and mount the shared folder via /etc/fstab
with something like

    file=/etc/fstab
    """
    10.0.2.2:/etc/italc /etc/italc nfs ro 0 0
    """


-- DISKLESS WORKSTATION --

We can do the samething we did for theworkstation: mount the /etc/italc folder
by fstab.


### RUN iTALC CLIENT (ica) ###

-- MAIN-SERVER PROFILE --

we do not need to run the daemon here. None have to control this
machine or use iTALC master on it (unless you run TJENER as a combi-server:
main-server, ltsp-server, workstation).

-- LTSP-SERVER PROFILE --

Thin clients run on this machine so we have to launch ica to control them.

Because we have to run "n" instances of ica for "n" thin clients
connected, we MUST use a different port for each ica session.
To achieve that sufficently, we call a small script instead of calling
directly /usr/bin/ica.

This script takes care of running ica using a unique port. One approach
can be that the port number is last part of the thin client IP plus 11.000.

(Note: on the master application to refer to a thin client we have to
specify the ltsp-server address WITH the unique port).

-- THIN CLIENT --

We do not have to do anything because we did it on the LTSP server.

-- WORKSTATION --

We do not need any modifications about ports here. We need only to
execute ica when the user logs in.

-- DISKLESS WORKSTATION --

The same as with workstations.



### iTALC MASTER CONFIGURATION ###

The iTALC master, like any other apllication, saves its own configuration
file in the user's (i.e. teacher's) home folder.

That means that teacher may configre it in the finest way but the other
teachers will have to repeat the same process.

We can avoid that using a global configuration file. We already
exported the folder /etc/italc. This folder is reachable by any host of the
Debian Edu netowrk, so we can simply put the configuration file into
this exported, global folder.

Copy the file configured by the teacher into that directory

    $ sudo cp ~/.italc/globalconfig.xml /etc/italc/

It may be a good idea to not give write permission to all teachers but
only to few teachers of group "admins".

    $ chown root:admins /etc/italc/globalconfig.xml
    $ chmod 664 /etc/italc/globalconfig.xml

Now we need to instruct iTALC to use that file. Edit /etc/xdg/iTALC Solutions/italc.conf
by adding

    file=/etc/xdg/iTALC Solutions/iTALC.conf
    [paths]
    globalconfig=/etc/italc/

###############################################################


Thanks to Valerio for this great piece of initial documentation!!!!

At the time of writing the whole setup is un-tested. This is on the Deban Edu
team's (actually mine) todo list.

light+love
Mike Gabriel,Vaumarcus CH, 2013-08-11
