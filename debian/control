Source: italc
Section: x11
Priority: optional
Maintainer: Debian Edu Packaging Team <debian-edu-pkg-team@lists.alioth.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
Build-Depends:
 cdbs,
 dpkg-dev (>= 1.16.1~),
 debhelper (>= 9),
 cmake,
 qtbase5-dev,
 qttools5-dev,
 qttools5-dev-tools,
 zlib1g-dev,
 libjpeg-dev,
 libpng-dev,
 imagemagick,
 libpam0g-dev,
 libavahi-client-dev,
 libssl-dev,
 libvncserver-dev (>= 0.9.7),
 libxdamage-dev,
 libxext-dev,
 libxfixes-dev,
 libxss-dev,
 libxtst-dev,
 libxrandr-dev,
 libxinerama-dev,
 libv4l-dev [linux-any],
 default-jdk,
Standards-Version: 4.1.1
Homepage: http://italc.sourceforge.net/home.php
Vcs-Git: https://salsa.debian.org/debian-edu-pkg-team/italc.git
Vcs-Browser: https://salsa.debian.org/debian-edu-pkg-team/italc

Package: italc-master
Architecture: any
Pre-Depends:
 dpkg (>= 1.15.6~),
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 python,
 italc-client,
 libitalccore (=${binary:Version}),
Description: intelligent Teaching And Learning with Computers - master
 iTALC makes it possible to access and guide the activities of students
 from the computer of the teacher. For example, with the help of iTALC
 a teacher can view the contents of students' screens and see if any of
 them need help. If so, the teacher can access the student's desktop and
 provide support; the student can watch the teacher's actions and learn
 from them. Alternatively the teacher can switch into "demo-mode", where
 all the students' screens show the contents of the teacher's screen.
 Furthermore, actions like locking students' screens, killing games,
 powering clients on or off, and much more can all be performed via iTALC.
 .
 This package contains the software necessary to observe and control iTALC
 clients provided by the italc-client package.

Package: italc-client
Architecture: any
Pre-Depends:
 dpkg (>= 1.15.6~),
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 adduser,
 libitalccore (=${binary:Version}),
 italc-management-console (=${binary:Version}),
Recommends:
 zenity | kdialog,
Description: intelligent Teaching And Learning with Computers - client
 iTALC makes it possible to access and guide the activities of students
 from the computer of the teacher. For example, with the help of iTALC
 a teacher can view the contents of students' screens and see if any of
 them need help. If so, the teacher can access the student's desktop and
 provide support; the student can watch the teacher's actions and learn
 from them. Alternatively the teacher can switch into "demo-mode", where
 all the students' screens show the contents of the teacher's screen.
 Furthermore, actions like locking students' screens, killing games,
 powering clients on or off, and much more can all be performed via iTALC.
 .
 This package contains the client software for iTALC, which can be
 controlled using italc-master.

Package: italc-management-console
Architecture: any
Pre-Depends:
 dpkg (>= 1.15.6~),
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libitalccore (=${binary:Version}),
Description: intelligent Teaching And Learning with Computers - management console
 iTALC makes it possible to access and guide the activities of students
 from the computer of the teacher. For example, with the help of iTALC
 a teacher can view the contents of students' screens and see if any of
 them need help. If so, the teacher can access the student's desktop and
 provide support; the student can watch the teacher's actions and learn
 from them. Alternatively the teacher can switch into "demo-mode", where
 all the students' screens show the contents of the teacher's screen.
 Furthermore, actions like locking students' screens, killing games,
 powering clients on or off, and much more can all be performed via iTALC.
 .
 This package contains the management console for iTALC, which helps to
 configure and manage iTALC installations.

Package: libitalccore
Architecture: any
Pre-Depends:
 dpkg (>= 1.15.6~),
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Multi-Arch: same
Provides:
 libitalc,
Breaks:
 libitalc,
Replaces:
 libitalc,
Description: intelligent Teaching And Learning with Computers - libraries
 iTALC makes it possible to access and guide the activities of students
 from the computer of the teacher. For example, with the help of iTALC
 a teacher can view the contents of students' screens and see if any of
 them need help. If so, the teacher can access the student's desktop and
 provide support; the student can watch the teacher's actions and learn
 from them. Alternatively the teacher can switch into "demo-mode", where
 all the students' screens show the contents of the teacher's screen.
 Furthermore, actions like locking students' screens, killing games,
 powering clients on or off, and much more can all be performed via iTALC.
 .
 This package provides the common libraries needed for iTALC.
